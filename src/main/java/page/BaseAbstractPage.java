package page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.*;
import utils.MobileDeviceScreen;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by apripachkin on 1/29/16.
 */
public abstract class BaseAbstractPage {
    protected AppiumDriver driver;
    protected MobileDeviceScreen mobileDeviceScreen;
    public BaseAbstractPage(AppiumDriver driver) {
        this.driver = driver;
        mobileDeviceScreen = new MobileDeviceScreen(driver);
    }

    private Match findElementOnScreen(String path) throws FindFailed {
        BufferedImage bufferedImage = Image.create(path).get();
        Pattern similar = new Pattern(bufferedImage);
        return mobileDeviceScreen.find(similar);
    }

    public void clickElement(String imgPath) throws FindFailed {
        Match elementOnScreen = findElementOnScreen(imgPath);
        preciseTap(elementOnScreen.getTarget().getX() / 2, elementOnScreen.getTarget().getY() / 2);
    }

    private void preciseTap(final int x, final int y) {
        TouchAction action = new TouchAction(driver);
        action.tap(x, y);
        action.perform();
    }
}
