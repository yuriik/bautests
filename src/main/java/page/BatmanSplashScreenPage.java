package page;

import io.appium.java_client.AppiumDriver;
import org.sikuli.script.*;

/**
 * Created by apripachkin on 1/29/16.
 */
public class BatmanSplashScreenPage extends BaseAbstractPage {
    public BatmanSplashScreenPage(AppiumDriver driver) {
        super(driver);
    }

    public BatmanPlayerBasePage proceedToMainScreen() throws  FindFailed {
        clickElement("src/main/resources/splash/logo.png");
        return new BatmanPlayerBasePage(driver);
    }
}
