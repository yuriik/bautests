package page;

import io.appium.java_client.AppiumDriver;
import org.sikuli.script.FindFailed;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by apripachkin on 2/2/16.
 */
public class BatmanPlayerBasePage extends BaseAbstractPage {
    private boolean isBlackMarketOpen = false;

    public BatmanPlayerBasePage(AppiumDriver driver) {
        super(driver);
    }

    public void openBlackMarket() throws FindFailed{
        clickElement("src/main/resources/playerBase/marketButton.png");
        isBlackMarketOpen = true;
    }

    public void buildVendingMachine() throws FindFailed {
        if (!isBlackMarketOpen) {
            openBlackMarket();
        }
        clickElement("src/main/resources/playerBase/blackMarket/buyVendingMachineButton.png");
        clickElement("src/main/resources/playerBase/confirm_button.png");
        clickElement("src/main/resources/playerBase/blackMarket/diamond_free_button.png");
    }
}
