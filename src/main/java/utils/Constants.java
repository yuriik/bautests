package utils;

/**
 * Created by apripachkin on 12/28/15.
 */
public class Constants {
    public static final String DEVICE_NAME = "Test iPhone";
    public static final String GAME_PACKAGE = "com.innovecs.wb.batman.dev";
    public static final String PLATFORM_IOS = "iOS";
    public static final String SERVER_URL = "http://127.0.0.1:4723/wd/hub/";
    public static final String IOS_VERSION = "8.3";
}
