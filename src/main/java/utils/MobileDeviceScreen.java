package utils;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.sikuli.script.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import org.sikuli.script.RobotDesktop;

/**
 * Created by apripachkin on 2/3/16.
 */
public class MobileDeviceScreen extends Region implements IScreen {
    private AppiumDriver driver;
    private ScreenImage lastScreenImage;
    private int currentId = -1;
    private RobotDesktop robot;

    public MobileDeviceScreen(AppiumDriver driver) {
        this.driver = driver;
        currentId = 0;
        initScreenBounds();
        initScreen(this);
    }

    private void initScreenBounds() {
        Rectangle bounds = getBounds();
        this.x = bounds.x;
        this.y = bounds.y;
        this.w = bounds.width;
        this.h = bounds.height;
    }

    public IRobot getRobot() {
        if (robot == null) {
            try {
                robot = new RobotDesktop();
            } catch (AWTException e) {
                e.printStackTrace();
            }
        }
        return robot;
    }

    public Rectangle getBounds() {
        WebDriver.Window window = driver.manage().window();
        int width = window.getSize().getWidth();
        int height = window.getSize().getHeight();
        int x = 0;
        int y = 0;
        return new Rectangle(x, y, width, height);
    }

    public ScreenImage capture() {
        return capture(getRect());
    }

    public ScreenImage capture(int x, int y, int w, int h) {
        Rectangle rect = newRegion(new Location(x, y), w, h).getRect();
        ScreenImage capture = capture(rect);
        lastScreenImage = capture;
        return capture;
    }

    public ScreenImage capture(Rectangle rect) {
        return new ScreenImage(rect, getScreenshot(rect));
    }

    public ScreenImage capture(Region reg) {
        return capture(reg.getRect());
    }

    public void showTarget(Location location) {
        //TODO add function
    }

    public int getID() {
        return currentId; // primary screen
    }

    public ScreenImage getLastScreenImageFromScreen() {
        return lastScreenImage;
    }

    public ScreenImage userCapture(String string) {
        return null;
    }

    public int getIdFromPoint(int srcx, int srcy) {
        return currentId;
    }

    private BufferedImage getScreenshot(Rectangle rectangle) {
        File screenshot = driver.getScreenshotAs(OutputType.FILE);
        try {
            return ImageIO.read(screenshot);
        } catch (IOException e) {
            return null;
        }
    }

    private BufferedImage crop(BufferedImage src, int x, int y, int width, int height) {
        return src.getSubimage(x, y, width, height);
    }

    public Region newRegion(Location loc, int width, int height) {
        return Region.create(loc.copyTo(this), width, height);
    }
}
