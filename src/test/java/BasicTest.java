import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import page.BatmanPlayerBasePage;
import page.BatmanSplashScreenPage;
import utils.Constants;
import utils.ProcessHandler;
import java.io.IOException;
import java.net.URL;

/**
 * Created by apripachkin on 1/28/16.
 */
public class BasicTest {
    private DesiredCapabilities desiredCapabilities;
    private AppiumDriver driver;
    @Before
    public void setUp() throws IOException {
        desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.UDID, ProcessHandler.deviceUDID());
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME,Constants.DEVICE_NAME);
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Constants.PLATFORM_IOS);
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, Constants.IOS_VERSION);
        desiredCapabilities.setCapability(MobileCapabilityType.APP, Constants.GAME_PACKAGE);
        desiredCapabilities.setCapability("orientation", "LANDSCAPE");
        desiredCapabilities.setCapability("screenshotWaitTimeout", 0.1);
        URL remoteAddress = new URL(Constants.SERVER_URL);
        driver = new IOSDriver(remoteAddress, desiredCapabilities);
        Settings.AutoWaitTimeout = 40.0F;
    }

    @Test
    public void openBlackMarket() throws FindFailed {
        BatmanSplashScreenPage batmanSplashScreenPage = new BatmanSplashScreenPage(driver);
        BatmanPlayerBasePage batmanPlayerBasePage = batmanSplashScreenPage.proceedToMainScreen();
        batmanPlayerBasePage.buildVendingMachine();
    }

}
